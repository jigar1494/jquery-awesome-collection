<script type="text/javascript" src="jquery.js"></script>
<!-- include the script -->
<script src="alertify.min.js"></script>

<!-- include the style -->
<link rel="stylesheet" href="css/alertify.min.css" />
<!-- include a theme -->
<link rel="stylesheet" href="css/themes/default.min.css" />


<script>



</script>

<script type="text/javascript">


//Simple Alert 
    $(document).ready(function () {
		
			alertify.alert('OK','Ready!');return;
			
			
			//Notification
			alertify.notify('sample', 'success', 6, function(){  console.log('dismissed'); });return;
			
			//Confirm Dialog
			//alertify.confirm('Confirm Title', 'Confirm Message', function(){ alertify.success('Ok') }
            //    , function(){ alertify.error('Cancel')});
				
				//Promt Dialog
				alertify.prompt( 'Prompt Title', 'Prompt Message', 'Prompt Value'
               , function(evt, value) { alertify.success('You entered: ' + value) }
               , function() { alertify.error('Cancel') });
				
				
			
    });
	//Errro Alert
	
	// Extend existing 'alert' dialog
if(!alertify.errorAlert){
  //define a new errorAlert base on alert
  alertify.dialog('errorAlert',function factory(){
    return{
            build:function(){
                var errorHeader = '<span class="fa fa-times-circle fa-2x" '
                +    'style="vertical-align:middle;color:#e10000;">'
                + '</span> Application Error';
                this.setHeader(errorHeader);
            }
        };
    },true,'alert');
}
//launch it.
// since this was transient, we can launch another instance at the same time.
alertify
    .errorAlert("This is a none singleton modal error alert! <br/><br/><br/>" +
        "To show another error alert: " + 
      "<a href='javascript:alertify.errorAlert(\"Another error\");'> Click here </a>");


	

</script>

