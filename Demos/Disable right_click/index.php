
<html>
<head>
<title>Disable Copy and Paste</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/autoScroll.js"></script>
<script>
//This script is to disable copy and paste.
	function disableCopy(e){
		return false
	}
	 
	function reEnable(){
		return true
	}
	 
	document.onselectstart=new Function ("return false")
	if (window.sidebar){
		document.onmousedown=disableCopy
		document.onclick=reEnable
	}

</script>
</head>
<body ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
<?php
$con=mysqli_connect("localhost","root","","auto_scroller");
		// Check connection
	if (mysqli_connect_errno()){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
?>
<div id="container">
	<div class="header">
    <h1 id="heading">Auto Scroll Animation</h1>
    </div>
    <div class="menu">
    <?php
    $result = mysqli_query($con, "SELECT * FROM content ORDER BY id DESC");
	while($row=mysqli_fetch_array($result)){
		$id = $row['id'];
		$title = $row['title'];
		$content = $row['content'];
	?>	
   <a href="#" onClick="return false;" onMouseDown="autoScrollTo('<?php echo $id; ?>');"><?php echo "<h2>" . $title . "</h2>"; ?></a>
	<?php
    }
	?>
	</div>
    <div class="content_wrapper">
    <?php
    $result_c = mysqli_query($con, "SELECT * FROM content ORDER BY id DESC");
	while($row_c=mysqli_fetch_array($result_c)){
		
    echo "<div id='" . $row_c['id'] . "' class='content'>";
    echo "<h3>" . $row_c['title'] . "</h3>"; 
    echo "<div class='content_holder'>" . $row_c['content'] . "</div>";
	?>
	<a href="#" onClick="return false;" onMouseDown="resetScroller('heading');"><?php echo "<h4><i>Go Bact To Top</i></h4>"; ?></a></h4>
    <?php
    echo "</div>";
 
	}
	?>
    </div>
</div>
<?php
mysqli_close($con);
?>
</body>
</html>
