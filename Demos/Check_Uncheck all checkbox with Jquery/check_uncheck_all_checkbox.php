<?php
/**
 * Created by PhpStorm.
 */
?>
<html>
<head>
<script type="text/javascript" src="jquery.js"></script>
    <link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
|

<div id="container">
    <div id="body">
        <div class="mainTitle" >Check/Uncheck all checkbox with Jquery</div>
        <div class="height20"></div>
        <article>
            <div class="height20"></div>
            <table class="bordered" >
                <tr>
                    <th width="10%"><input type="checkbox" name="chk_all" class="chk_all"></th>
                    <th >Country</th>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="1" ></td>
                    <td style="text-align:center;"> India </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="2" ></td>
                    <td style="text-align:center;"> Pakistan </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="3" ></td>
                    <td style="text-align:center;"> Sri Lanka </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="4" ></td>
                    <td style="text-align:center;"> Bangladesh </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="5" ></td>
                    <td style="text-align:center;"> England </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="6" ></td>
                    <td style="text-align:center;"> Australia </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="7" ></td>
                    <td style="text-align:center;"> United States of America </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="8" ></td>
                    <td style="text-align:center;"> Soth Africa </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="9" ></td>
                    <td style="text-align:center;"> Zimbabwe </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="10" ></td>
                    <td style="text-align:center;"> Afganistan </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="10" ></td>
                    <td style="text-align:center;"> Iraq </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="11" ></td>
                    <td style="text-align:center;"> Indonesia </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="country_id" class="checkboxes" value="12" ></td>
                    <td style="text-align:center;"> Switzerland </td>
                </tr>
            </table>
        </article>
        <div class="height10"></div>

    </div>
</div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {

        // binding the check all box to chk_all click event
        $(".chk_all").click(function () {

            var checkAll = $(".chk_all").prop('checked');
            if (checkAll) {
                $(".checkboxes").prop("checked", true);
            } else {
                $(".checkboxes").prop("checked", false);
            }

        });

        // if all checkbox are selected, check the chk_all checkbox class and vise versa
        $(".checkboxes").click(function ()
        {
            if($(".checkboxes").length==$(".subscheked:checked").length)
            {
                $(".chk_all").attr("checked", "checked");
            }
            else
            {
                $(".chk_all").removeAttr("checked");
            }
        });

    });
</script>