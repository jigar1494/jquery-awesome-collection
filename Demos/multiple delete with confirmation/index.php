<?php
	$con = mysqli_connect("localhost","root","","multiple_delete");
	if (mysqli_connect_errno()){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = mysqli_query($con, "SELECT * FROM content");
?>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="css/style.css">
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery.confirm.min.js"></script>
<title></title>
</head>
<body>
	<div id="container">
    	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    	<div class="content">
            	<table>
                	<tr>
                        <th align="left"><input type="checkbox" id="check_all" name="check_all">Check All</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                       	<th>Last Name</th>
                    </tr>
                     <?php 
						if(mysqli_num_rows($query) >= 1){
                     		while($row = (mysqli_fetch_array($query))){ 
					 ?>
                	<tr>
                        <td><input type="checkbox" class="multiple" name="multiple[]" value="<?php echo $row['id']; ?>"></td>
                        <td><?php echo $row['fname']; ?></td>
                        <td><?php echo $row['mname']; ?></td>
                       	<td><?php echo $row['lname']; ?></td>
                    </tr>
                    <?php 
							}
	  					}else{
							echo "<td>No data stored in your database!</td>";
						}
					?>
                </table>
        </div>
        <input id="confirm" type="submit" name="delete" value="Delete">
        
        <script>
			$(document).ready(function(){
				$("#confirm").confirm({
					text: "Are you sure you want to delete selected files?",
					title: "Confirm",
					confirm: function() {
						var length = $(':checkbox:checked').length;
						if(length >= 1){
							var val = [];
							$(':checkbox:checked').each(function(i){
								val[i] = $(this).val();
							});
							$.post('delete.php?delete&', {id: val}, function(){
									location.reload();
							});
						}else{
							alert("Select a file to delete!");
						}
					},
					confirmButton: "Confirm Deletion",
					cancelButton: "Cancel",
					post: true
				});
			});
		</script>
    </div>
<script type="text/javascript" src="js/checkall.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
</body>
</html>