(function($){

/*Strict mode is a feature that places the browser in a “strict” operating mode
*
* •	Prevents access to “unsafe” actions, such as gaining access to a global object.
* */
    "use strict";

    // Global Variables
    var MAX_HEIGHT = 100;

    $.blogPost = function(el, options) {

        // Private Functions
        function debug(e) {
            console.log(e);
        }

        // Global Private Variables
        var MAX_WIDTH = 200;

        var base = this;

        base.$el = $(el);
        base.el = el;

        base.$el.data('blogPost', base);

        /*'init' function will be the first thing called when our plug-in starts up. */
        base.init = function(){

            var totalButtons = 0;

            base.$el.append('<button name="public">Private</button>');
            base.$el.append('<button name="private">Public</button>');

        };

        base.clicker = function(e) {
            debug(e);
        };

        base.init();
    };

    $.fn.blogPost = function(options){
        debugger;
        return this.each(function(){
            var bp = new $.blogPost(this, options);

            $('button[name="public"]').click(function(e) {
                bp.clicker(e);
            });
            $('button[name="private"]').click(function(e) {
                bp.debug(e);
            });
        });
    }




})(jQuery);