(function ($) {
    //defines the name for our plugin-hello
    $.fn.hello = function (options) {

        // This is the easiest way to have default options
        var setting = $.extend({

            name: 'Jigar Kumar',
            color: 'red'
        }, options);

        //Apply options

        console.log(setting);

        return this.append('Hello ' + setting.name + '!').css({ color: setting.color });


    }


}(jQuery));