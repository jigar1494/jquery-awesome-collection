/* 

	jLoader  - a jQuery Image Loader
	
	Copyright (c) 2009 Dan Wellman
	
	Dual licensed under the MIT and GPL licenses:
  http://www.opensource.org/licenses/mit-license.php
  http://www.gnu.org/licenses/gpl.html
	
*/

(function($) {

	//define jLoader object with some default config settings
	$.jLoader = {
		defaults: {
			imgDir: "loaderImages/",
			imgContainer: "",
			imgTotal: 9,
			imgFormat: ".jpg",
			simpleFileNames: true
		}
	};
	
	//extend jquery with the plugin
	$.fn.extend({
		jLoader:function(config, fileNames) {
			
			//use defaults or properties supplied by user
			var config = $.extend({}, $.jLoader.defaults, config);
			
			//get the id of the selected element
			config.imgContainer = this.attr("id");
			
			//use simpleLoad to load numerically named images or complexLoad for user-specified file names
			(config.simpleFileNames == true) ? simpleLoad(config) : complexLoad(config, fileNames) ;
			
			//return the jquery object for chaining
			return this;
		}
	});
	
  //simple image load
	function simpleLoad(config) {
		
		//loop through images and load sequentially
		for (var x = 1; x < config.imgTotal + 1; x++) {
			
			//create image
			$("<img />").attr({ 
				id: "image" + x, 
				src: config.imgDir + x + config.imgFormat,
				title: "Image" + x
			}).appendTo("#" + config.imgContainer).css({ display: "none" });			
		}
	};
	
	//complex image load
	function complexLoad(config, fileNames) {
		
		//loop through the array in the second argument
		for (var x = 0; x < fileNames.length; x++) {
				
				//create image
				$("<img />").attr({
					id: fileNames[x],
					src: config.imgDir + fileNames[x] + config.imgFormat,
					title: "The " + fileNames[x] + " nebula"
				}).appendTo("#" + config.imgContainer).css({ display: "none" });
		}
	};
	
})(jQuery);