

(function($){

    $.jLoader  = {
        defaults:{
            imgDir: "images/",
            imgContainer: "",
            imgTotal: 9,
            imgFormat: ".jpg",
            simpleFileNames: true
        }
    };


    $.fn.extend({

        jLoader:function(config,fileNames){

            var config = $.extend({}, $.jLoader.defaults, config);

            config.imgContainer = this.attr("id");

            (config.simpleFileNames == true) ? simpleLoad(config) : complexLoad(config, fileNames) ;

            return this;
        }
    });

    function simpleLoad(config) {
        for (var x = 1; x < config.imgTotal + 1; x++) {
            $("<img />").attr({
                id: "image" + x,
                src: config.imgDir + x + config.imgFormat,
                title: "Image" + x
            }).appendTo("#" + config.imgContainer).css({ display: "none" });
        }
    };


    function complexLoad(config, fileNames) {
        for (var x = 0; x < fileNames.length; x++) {
            $("<img />").attr({
                id: fileNames[x],
                src: config.imgDir + fileNames[x] + config.imgFormat,
                title: "The " + fileNames[x] + " nebula"
            }).appendTo("#" + config.imgContainer).css({ display: "none" });
        }
    };


})(jQuery);